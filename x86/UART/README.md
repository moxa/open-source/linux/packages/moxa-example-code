# UART Control

## Usage

```
Usage:
                mx-uart-ctl -p <port_number> [-m <uart_mode>]

OPTIONS:
                -p <port_number>
                                Set target port.
                -m <uart_mode>
                                Set target port to uart_mode
                                0 --> set to RS232 mode
                                1 --> set to RS485-2W mode
                                2 --> set to RS422/RS485-4W mode

Example:
                Get mode from port 0
                # mx-uart-ctl -p 0

                Set port 1 to mode RS232
                # mx-uart-ctl -p 1 -m 0
```

## Support Models

- DA series
  - DA-820C
  - DA-682C
  - DA-681C
- V series
  - V3000
  - V2406C
- MC series
  - MC-3201
