# setinterface

```
Usage: setinterface device-node [interface-no]
        device-node     - /dev/ttyM0 ~ /dev/ttyM7
        interface-no    - following:
        none - to view now setting
        0 - set to RS232 interface
        1 - set to RS485-2WIRES interface
        2 - set to RS422 interface
        3 - set to RS485-4WIRES interface
```

## Support Models

- DA series
  - DA-720 (on **DE-SP08-I-TB** UART expansion modules, support RS232/RS485-2WIRES/RS422 mode select)
