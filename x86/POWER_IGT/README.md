# Power ignition management

Power ignition management is the controller that manages power distribution to\
in-vehicle computers by receiving signals when starting the ignition system of\
transportation vehicles.

## Config example
- POWER_IGT_GPIO: the super IO gpio pin number of power ignition, e.g. GP85
- POWER_IGT_SIG_OFF_VAL: the power ignition signal off status
- POWER_IGT_DELAY_SEC: the delay time (sec) of daemon

edit: `mx-power-igtd`
```
POWER_IGT_GPIO="85"
POWER_IGT_SIG_OFF_VAL="0"
POWER_IGT_DELAY_SEC="1"
```

## Support Models

- V series
  - V2403C
