# DIO Control

## Usage

```
Usage:
        mx-dio-ctl <-i|-o <#port number> [-s <#state>]>

OPTIONS:
        -i <#DIN port number>
        -o <#DOUT port number>
        -s <#state>
                Set state for target DOUT port
                0 --> LOW
                1 --> HIGH

Example:
        Get value from DIN port 0
        # mx-dio-ctl -i 0
        Get value from DOUT port 0
        # mx-dio-ctl -o 0

        Set DOUT port 0 value to LOW
        # mx-dio-ctl -o 0 -s 0
        Set DOUT port 0 value to HIGH
        # mx-dio-ctl -o 0 -s 1
```

## Support Models

- DA series
  - DA-820C
  - DA-682C
  - DA-681C
- V series
  - V3000
